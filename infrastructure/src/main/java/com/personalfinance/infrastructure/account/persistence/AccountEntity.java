package com.personalfinance.infrastructure.account.persistence;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.personalfinance.domain.account.Account;
import com.personalfinance.domain.account.AccountID;

@Entity
@Table(name = "accounts")
public class AccountEntity {

    public AccountEntity() {
    }

    public AccountEntity(UUID id, UUID userId, BigDecimal balance) {
        this.id = id;
        this.userId = userId;
        this.balance = balance;
    }

    @Id
    private UUID id;

    @Column(name = "user_id", nullable = false)
    private UUID userId;

    @Column(name = "balance", nullable = false)
    private BigDecimal balance;

    public UUID getId() {
        return this.id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getUserId() {
        return this.userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public BigDecimal getBalance() {
        return this.balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Account toAggregate() {
        return Account.with(AccountID.from(this.id), userId, balance);
    }

    public static AccountEntity fromDomain(Account account) {
        final var anId = UUID.fromString(account.getId().getValue());
        return new AccountEntity(anId, account.getUserId(), account.getBalance());
    }

}
