package com.personalfinance.infrastructure.account.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public record CreateAccountRequest(
    @JsonProperty("user_id") String userId
) {
    
}
