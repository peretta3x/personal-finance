package com.personalfinance.infrastructure.account;

import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Repository;

import com.personalfinance.domain.account.Account;
import com.personalfinance.domain.account.AccountGateway;
import com.personalfinance.domain.account.AccountID;
import com.personalfinance.infrastructure.account.persistence.AccountEntity;
import com.personalfinance.infrastructure.account.persistence.AccountRepository;

@Repository
public class AccountPostgresGateway implements AccountGateway {

    private final AccountRepository accountJPARepository;

    public AccountPostgresGateway(AccountRepository jpaRepository) {
        this.accountJPARepository = jpaRepository;
    }

    @Override
    public Account create(final Account account) {
        final var anId = UUID.fromString(account.getId().getValue());
        AccountEntity accountEntity = new AccountEntity(anId, account.getUserId(), account.getBalance());
        final var accountCreated = this.accountJPARepository.save(accountEntity).toAggregate();
        return accountCreated;
    }

    @Override
    public Optional<Account> getById(final AccountID id) {
        return this.accountJPARepository.findById(UUID.fromString(id.getValue()))
            .map(AccountEntity::toAggregate);
    }

    @Override
    public Optional<Account> getByUserId(final UUID userId) {
        return this.accountJPARepository.findByUserId(userId).map(AccountEntity::toAggregate);
    }

    @Override
    public Account save(Account account) {
        final var anId = UUID.fromString(account.getId().getValue());
        final var accountEntity = new AccountEntity(anId, account.getUserId(), account.getBalance());
        return this.accountJPARepository.save(accountEntity).toAggregate();
    }

}
