package com.personalfinance.infrastructure.account.persistence;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<AccountEntity, UUID> {

    public Optional<AccountEntity> findById(UUID id);

    public Optional<AccountEntity> findByUserId(UUID id);

}
