package com.personalfinance.infrastructure.configuration.account;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.personalfinance.application.usecases.account.create.CreateAccountUseCase;
import com.personalfinance.application.usecases.account.create.DefaultCreateAccountUseCase;
import com.personalfinance.application.usecases.account.get.DefaultGetAccountByUserIdUseCase;
import com.personalfinance.application.usecases.account.get.GetAccountByUserIdUseCase;
import com.personalfinance.domain.account.AccountGateway;

@Configuration
public class AccountConfiguration {

    private final AccountGateway accountGateway;

    public AccountConfiguration(final AccountGateway accountGateway) {
        this.accountGateway = accountGateway;
    }
    
    @Bean
    public CreateAccountUseCase createAccountUseCase() {
        return new DefaultCreateAccountUseCase(accountGateway);
    }

    @Bean
    public GetAccountByUserIdUseCase readAccountUseCase() {
        return new DefaultGetAccountByUserIdUseCase(accountGateway);
    }

}
