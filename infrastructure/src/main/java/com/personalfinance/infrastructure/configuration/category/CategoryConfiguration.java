package com.personalfinance.infrastructure.configuration.category;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.personalfinance.application.usecases.category.create.CreateCategoryUseCase;
import com.personalfinance.application.usecases.category.create.DefaultCreateCategoryUseCase;
import com.personalfinance.application.usecases.category.findall.DefaultFindAllCategoriesUseCase;
import com.personalfinance.application.usecases.category.findall.FindAllCategoriesUseCase;
import com.personalfinance.application.usecases.category.get.DefaultFindCategoryByIdUseCase;
import com.personalfinance.application.usecases.category.get.FindCategoryByIdUseCase;
import com.personalfinance.application.usecases.category.remove.DefaultRemoveCategoryUseCase;
import com.personalfinance.application.usecases.category.remove.RemoveCategoryUseCase;
import com.personalfinance.application.usecases.category.update.DefaultUpdateCategoryUseCase;
import com.personalfinance.application.usecases.category.update.UpdateCategoryUseCase;
import com.personalfinance.domain.category.CategoryGateway;

@Configuration
public class CategoryConfiguration {

    private final CategoryGateway categoryGateway;

    public CategoryConfiguration(final CategoryGateway categoryGateway) {
        this.categoryGateway = categoryGateway;
    }

    @Bean
    public CreateCategoryUseCase createCategoryUseCase() {
        return new DefaultCreateCategoryUseCase(categoryGateway);
    }

    @Bean
    public FindAllCategoriesUseCase findAllCategoriesUseCase() {
        return new DefaultFindAllCategoriesUseCase(categoryGateway);
    }

    @Bean
    public FindCategoryByIdUseCase findCategoryByIdUseCase() {
        return new DefaultFindCategoryByIdUseCase(categoryGateway);
    }

    @Bean
    public RemoveCategoryUseCase removeCategoryUseCase() {
        return new DefaultRemoveCategoryUseCase(categoryGateway);
    }

    @Bean
    public UpdateCategoryUseCase updateCategoryUseCase() {
        return new DefaultUpdateCategoryUseCase(categoryGateway);
    }
}
