package com.personalfinance.infrastructure.configuration.transaction;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.personalfinance.application.usecases.transaction.create.DefaultCreateTransactionUseCase;
import com.personalfinance.application.usecases.transaction.findall.DefaultFindAllTransactionsByAccountUseCase;
import com.personalfinance.application.usecases.transaction.get.DefaultFindTransactionByIdUseCase;
import com.personalfinance.application.usecases.transaction.get.FindTransactionByIdUseCase;
import com.personalfinance.application.usecases.transaction.remove.DefaultRemoveTransactionUseCase;
import com.personalfinance.application.usecases.transaction.remove.RemoveTransactionUseCase;
import com.personalfinance.application.usecases.transaction.search.DefaultSearchTransactionsUseCase;
import com.personalfinance.application.usecases.transaction.search.SearchTransactionUseCase;
import com.personalfinance.application.usecases.transaction.update.DefaultUpdateTransactionUseCase;
import com.personalfinance.application.usecases.transaction.update.UpdateTransactionUseCase;
import com.personalfinance.domain.account.AccountGateway;
import com.personalfinance.domain.category.CategoryGateway;
import com.personalfinance.domain.transaction.TransactionGateway;

@Configuration
public class TransactionConfiguration {

    private final TransactionGateway transactionGateway;
    private final AccountGateway accountGateway;
    private final CategoryGateway categoryGateway;

    public TransactionConfiguration(
            final TransactionGateway transactionGateway,
            final AccountGateway accountGateway,
            final CategoryGateway categoryGateway) {
        this.transactionGateway = transactionGateway;
        this.accountGateway = accountGateway;
        this.categoryGateway = categoryGateway;
    }

    @Bean
    public DefaultCreateTransactionUseCase createTransactionUseCase() {
        return new DefaultCreateTransactionUseCase(accountGateway, categoryGateway, transactionGateway);
    }

    @Bean
    public DefaultFindAllTransactionsByAccountUseCase findAllTransactionsByAccountUseCase() {
        return new DefaultFindAllTransactionsByAccountUseCase(transactionGateway);
    }

    @Bean
    public RemoveTransactionUseCase removeTransactionUseCase() {
        return new DefaultRemoveTransactionUseCase(transactionGateway, accountGateway);
    }

    @Bean
    public UpdateTransactionUseCase updateTransactionUseCase() {
        return new DefaultUpdateTransactionUseCase(transactionGateway, accountGateway);
    }

    @Bean
    public FindTransactionByIdUseCase findTransactionByIdUseCase() {
        return new DefaultFindTransactionByIdUseCase(transactionGateway);
    }

    @Bean
    public SearchTransactionUseCase searchTransactionUseCase() {
        return new DefaultSearchTransactionsUseCase(transactionGateway);
    }
}
