package com.personalfinance.infrastructure.api.controllers.category;

import com.personalfinance.application.usecases.category.common.CategoryOutput;
import com.personalfinance.application.usecases.category.create.CategoryInput;
import com.personalfinance.application.usecases.category.create.CreateCategoryUseCase;
import com.personalfinance.application.usecases.category.findall.FindAllCategoriesUseCase;
import com.personalfinance.application.usecases.category.get.FindCategoryByIdUseCase;
import com.personalfinance.application.usecases.category.remove.RemoveCategoryUseCase;
import com.personalfinance.application.usecases.category.update.UpdateCategoryInput;
import com.personalfinance.application.usecases.category.update.UpdateCategoryUseCase;
import com.personalfinance.infrastructure.category.models.CreateCategoryRequest;
import com.personalfinance.infrastructure.category.models.UpdateCategoryRequest;
import java.util.List;
import java.util.UUID;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoryController implements CategoryAPI {

    private final CreateCategoryUseCase createCategoryUseCase;
    private final FindAllCategoriesUseCase findAllCategoriesUseCase;
    private final FindCategoryByIdUseCase findCategoryByIdUseCase;
    private final RemoveCategoryUseCase removeCategoryUseCase;
    private final UpdateCategoryUseCase updateCategoryUseCase;

    public CategoryController(
        final CreateCategoryUseCase createCategoryUseCase,
        final FindAllCategoriesUseCase findAllCategoriesUseCase,
        final FindCategoryByIdUseCase findCategoryByIdUseCase,
        final RemoveCategoryUseCase removeCategoryUseCase,
        final UpdateCategoryUseCase updateCategoryUseCase
    ) {
        this.createCategoryUseCase = createCategoryUseCase;
        this.findAllCategoriesUseCase = findAllCategoriesUseCase;
        this.findCategoryByIdUseCase = findCategoryByIdUseCase;
        this.removeCategoryUseCase = removeCategoryUseCase;
        this.updateCategoryUseCase = updateCategoryUseCase;
    }

    @Override
    public CategoryOutput create(CreateCategoryRequest createCategoryRequest) {
        final var input = CategoryInput.with(createCategoryRequest.name());
        return createCategoryUseCase.execute(input);
    }

    @Override
    public List<CategoryOutput> findAll() {
        return findAllCategoriesUseCase.execute();
    }

    @Override
    public CategoryOutput findById(UUID id) {
        return findCategoryByIdUseCase.execute(id);
    }

    @Override
    public void removeById(UUID id) {
        removeCategoryUseCase.execute(id);
    }

    @Override
    public void update(final UUID id, final UpdateCategoryRequest input) {
        updateCategoryUseCase.execute(new UpdateCategoryInput(id, input.name()));
    }

}
