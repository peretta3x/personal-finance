package com.personalfinance.infrastructure.api.controllers.category;

import java.util.List;
import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.personalfinance.application.usecases.category.common.CategoryOutput;
import com.personalfinance.infrastructure.category.models.CreateCategoryRequest;
import com.personalfinance.infrastructure.category.models.UpdateCategoryRequest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RequestMapping("categories")
public interface CategoryAPI {

        @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseStatus(code = HttpStatus.CREATED)
        @Operation(summary = "Create a new category")
        @ApiResponses(value = {
                        @ApiResponse(responseCode = "201", description = "Created successfully"),
                        @ApiResponse(responseCode = "422", description = "A validation error was thrown"),
                        @ApiResponse(responseCode = "500", description = "Internal Server Error")
        })
        CategoryOutput create(@RequestBody CreateCategoryRequest request);

        @GetMapping
        @Operation(summary = "Find all categories")
        @ApiResponses(value = {
                        @ApiResponse(responseCode = "200", description = "List Success"),
                        @ApiResponse(responseCode = "500", description = "Internal Server Error")
        })
        List<CategoryOutput> findAll();

        @GetMapping("{id}")
        @Operation(summary = "Find category by id")
        @ApiResponses(value = {
                        @ApiResponse(responseCode = "200", description = "Find Success"),
                        @ApiResponse(responseCode = "500", description = "Internal Server Error")
        })
        CategoryOutput findById(@PathVariable UUID id);

        @DeleteMapping("{id}")
        @ResponseStatus(code = HttpStatus.NO_CONTENT)
        @Operation(summary = "Remove category by id")
        @ApiResponses(value = {
                        @ApiResponse(responseCode = "204", description = "Remove Success"),
                        @ApiResponse(responseCode = "500", description = "Internal Server Error")
        })
        void removeById(@PathVariable UUID id);

        @PutMapping("{id}")
        @ResponseStatus(code = HttpStatus.NO_CONTENT)
        @Operation(summary = "Update category by id")
        @ApiResponses(value = {
                        @ApiResponse(responseCode = "204", description = "Update Success"),
                        @ApiResponse(responseCode = "500", description = "Internal Server Error")
        })
        void update(@PathVariable UUID id, @RequestBody UpdateCategoryRequest input);
}
