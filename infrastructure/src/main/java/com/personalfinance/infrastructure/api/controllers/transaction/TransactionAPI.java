package com.personalfinance.infrastructure.api.controllers.transaction;

import java.util.List;
import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import com.personalfinance.application.usecases.transaction.common.TransactionOutput;
import com.personalfinance.application.usecases.transaction.create.CreateTransactionOutput;
import com.personalfinance.domain.pagination.Pagination;
import com.personalfinance.infrastructure.transaction.models.CreateTransactionRequest;
import com.personalfinance.infrastructure.transaction.models.UpdateTransactionRequest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RequestMapping("transactions")
public interface TransactionAPI {
        @PostMapping
        @ResponseStatus(code = HttpStatus.CREATED)
        @Operation(summary = "Create a new transaction")
        @ApiResponses(value = {
                        @ApiResponse(responseCode = "201", description = "Created successfully"),
                        @ApiResponse(responseCode = "500", description = "An internal server error was thrown")
        })
        CreateTransactionOutput create(@RequestBody CreateTransactionRequest createTransactionRequest);

        @GetMapping
        @ResponseStatus(code = HttpStatus.OK)
        @Operation(summary = "Find all transactions")
        @ApiResponses(value = {
                        @ApiResponse(responseCode = "200", description = "List Success"),
                        @ApiResponse(responseCode = "500", description = "An internal server error was thrown")
        })
        List<TransactionOutput> findAll(@RequestParam("account_id") UUID accountId);

        @GetMapping("/search")
        @ResponseStatus(code = HttpStatus.OK)
        @Operation(summary = "Search for transactions")
        @ApiResponses(value = {
                        @ApiResponse(responseCode = "200", description = "Search Successed"),
                        @ApiResponse(responseCode = "500", description = "An internal server error was thrown")
        })
        Pagination<?> search(
                @RequestParam(name = "account_id", required = false, defaultValue = "") final String accountId,
                @RequestParam(name = "page", required = false, defaultValue = "0") final int page,
                @RequestParam(name = "per_page", required = false, defaultValue = "10") final int perPage,
                @RequestParam(name = "month", required = false) final Integer month,
                @RequestParam(name = "sort", required = false, defaultValue = "date") final String sort,
                @RequestParam(name = "dir", required = false, defaultValue = "asc") final String direction
        );

        @GetMapping("{id}")
        @ResponseStatus(code = HttpStatus.OK)
        @Operation(summary = "Find transaction by id")
        @ApiResponses(value = {
                        @ApiResponse(responseCode = "200", description = "Find Success"),
                        @ApiResponse(responseCode = "500", description = "Internal Server Error")
        })
        TransactionOutput findById(@PathVariable UUID id);

        @DeleteMapping("{id}")
        @ResponseStatus(code = HttpStatus.NO_CONTENT)
        @Operation(summary = "Remove transaction by id")
        @ApiResponses(value = {
                        @ApiResponse(responseCode = "204", description = "Remove Success"),
                        @ApiResponse(responseCode = "500", description = "Internal Server Error")
        })
        void removeById(@PathVariable UUID id);

        @PutMapping("{id}")
        @ResponseStatus(code = HttpStatus.NO_CONTENT)
        @Operation(summary = "Update transaction by id")
        @ApiResponses(value = {
                        @ApiResponse(responseCode = "204", description = "Update Success"),
                        @ApiResponse(responseCode = "500", description = "Internal Server Error")
        })
        void update(@PathVariable UUID id, @RequestBody UpdateTransactionRequest updateTransactionRequest);
}
