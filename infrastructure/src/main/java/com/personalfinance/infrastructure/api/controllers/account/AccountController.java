package com.personalfinance.infrastructure.api.controllers.account;

import java.util.UUID;

import org.springframework.web.bind.annotation.RestController;

import com.personalfinance.application.usecases.account.common.AccountOutput;
import com.personalfinance.application.usecases.account.create.CreateAccountUseCase;
import com.personalfinance.application.usecases.account.get.GetAccountByUserIdUseCase;
import com.personalfinance.infrastructure.account.model.CreateAccountRequest;

@RestController
public class AccountController implements AccountAPI {

    private final CreateAccountUseCase createAccountUseCase;
    private final GetAccountByUserIdUseCase readAccountUseCase;

    public AccountController(
        final CreateAccountUseCase createAccountUseCase,
        final GetAccountByUserIdUseCase readAccountUseCase
    ) {
        this.createAccountUseCase = createAccountUseCase;
        this.readAccountUseCase = readAccountUseCase;
    }

    @Override
    public AccountOutput create(final CreateAccountRequest request) {
        final var userID = UUID.fromString(request.userId());
        return this.createAccountUseCase.execute(userID);
    }

    @Override
    public AccountOutput find(final String id) {
        final var userId = UUID.fromString(id);
        return this.readAccountUseCase.execute(userId);
    }
}