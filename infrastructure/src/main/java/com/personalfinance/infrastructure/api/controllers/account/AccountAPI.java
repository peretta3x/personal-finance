package com.personalfinance.infrastructure.api.controllers.account;

import javax.websocket.server.PathParam;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.personalfinance.application.usecases.account.common.AccountOutput;
import com.personalfinance.infrastructure.account.model.CreateAccountRequest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RequestMapping("accounts")
public interface AccountAPI {

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(code = HttpStatus.CREATED)
    @Operation(summary = "Create a new account")
    @ApiResponses(value = {
                    @ApiResponse(responseCode = "201", description = "Created successfully"),
                    @ApiResponse(responseCode = "422", description = "A validation error was thrown"),
                    @ApiResponse(responseCode = "500", description = "Internal Server Error")
    })
    AccountOutput create(@RequestBody CreateAccountRequest createAccountRequest);

    
    @GetMapping("{id}")
    @Operation(summary = "Find account by id")
    @ApiResponses(value = {
                    @ApiResponse(responseCode = "200", description = "Find Success"),
                    @ApiResponse(responseCode = "500", description = "Internal Server Error")
    })
    AccountOutput find(@PathParam("id") String id);

}
