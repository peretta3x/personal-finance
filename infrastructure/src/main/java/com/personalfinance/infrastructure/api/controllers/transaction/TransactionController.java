package com.personalfinance.infrastructure.api.controllers.transaction;

import java.util.List;
import java.util.UUID;

import org.springframework.web.bind.annotation.RestController;

import com.personalfinance.application.usecases.transaction.common.TransactionOutput;
import com.personalfinance.application.usecases.transaction.create.CreateTransactionInput;
import com.personalfinance.application.usecases.transaction.create.CreateTransactionOutput;
import com.personalfinance.application.usecases.transaction.create.DefaultCreateTransactionUseCase;
import com.personalfinance.application.usecases.transaction.findall.DefaultFindAllTransactionsByAccountUseCase;
import com.personalfinance.application.usecases.transaction.findall.FindAllTransactionsInput;
import com.personalfinance.application.usecases.transaction.get.FindTransactionByIdUseCase;
import com.personalfinance.application.usecases.transaction.remove.RemoveTransactionUseCase;
import com.personalfinance.application.usecases.transaction.search.SearchTransactionUseCase;
import com.personalfinance.application.usecases.transaction.update.UpdateTransactionInput;
import com.personalfinance.application.usecases.transaction.update.UpdateTransactionUseCase;
import com.personalfinance.domain.pagination.Pagination;
import com.personalfinance.domain.transaction.TransactionSearchQuery;
import com.personalfinance.domain.transaction.TransactionType;
import com.personalfinance.infrastructure.transaction.models.CreateTransactionRequest;
import com.personalfinance.infrastructure.transaction.models.UpdateTransactionRequest;
import com.personalfinance.infrastructure.transaction.presenters.TransactionApiPresenter;

@RestController
public class TransactionController implements TransactionAPI {

    private final DefaultCreateTransactionUseCase createTransactionUseCase;
    private final DefaultFindAllTransactionsByAccountUseCase findAllTransactionsByAccountUseCase;
    private final RemoveTransactionUseCase removeTransactionUseCase;
    private final UpdateTransactionUseCase updateTransactionUseCase;
    private final FindTransactionByIdUseCase findTransactionByIdUseCase;
    private final SearchTransactionUseCase searchTransactionUseCase;

    public TransactionController(
        final DefaultCreateTransactionUseCase createTransactionUseCase,
        final DefaultFindAllTransactionsByAccountUseCase findAllTransactionsByAccountUseCase,
        final RemoveTransactionUseCase removeTransactionUseCase, 
        final UpdateTransactionUseCase updateTransactionUseCase,
        final FindTransactionByIdUseCase findTransactionByIdUseCase,
        final SearchTransactionUseCase searchTransactionUseCase
    ) {
        this.createTransactionUseCase = createTransactionUseCase;
        this.findAllTransactionsByAccountUseCase = findAllTransactionsByAccountUseCase;
        this.removeTransactionUseCase = removeTransactionUseCase;
        this.updateTransactionUseCase = updateTransactionUseCase;
        this.findTransactionByIdUseCase = findTransactionByIdUseCase;
        this.searchTransactionUseCase = searchTransactionUseCase;
    }

    @Override
    public CreateTransactionOutput create(final CreateTransactionRequest request) {
        final var input = new CreateTransactionInput(
            request.userId(),
            request.description(),
            request.categoryId(),
            request.value(),
            TransactionType.valueOf(request.type()),
            request.date()
        );
        return this.createTransactionUseCase.execute(input);
    }

    @Override
    public List<TransactionOutput> findAll(final UUID accountId) {
        return this.findAllTransactionsByAccountUseCase.execute(new FindAllTransactionsInput(accountId));
    }

    @Override
    public void removeById(UUID id) {
        this.removeTransactionUseCase.execute(id);
    }

    @Override
    public void update(final UUID id, final UpdateTransactionRequest request) {
        final var input = new UpdateTransactionInput(
            id,
            request.description(),
            request.categoryId(),
            request.value(),
            TransactionType.valueOf(request.type()),
            request.date()
        );
        this.updateTransactionUseCase.execute(input);
    }

    @Override
    public TransactionOutput findById(UUID id) {
        return this.findTransactionByIdUseCase.execute(id);
    }

    @Override
    public Pagination<?> search(
        String accountId, 
        int page, 
        int perPage, 
        Integer month, 
        String sort,   
        String direction
    ) {
        final var query = new TransactionSearchQuery(
            page,
            perPage,
            UUID.fromString(accountId),
            7,
            sort,
            direction
        );
        return this.searchTransactionUseCase.execute(query)
                .map(TransactionApiPresenter::present);
    }

}
