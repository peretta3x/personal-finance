package com.personalfinance.infrastructure.transaction;

import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import com.personalfinance.domain.pagination.Pagination;
import com.personalfinance.domain.transaction.Transaction;
import com.personalfinance.domain.transaction.TransactionGateway;
import com.personalfinance.domain.transaction.TransactionSearchQuery;
import com.personalfinance.infrastructure.transaction.persistence.TransactionEntity;
import com.personalfinance.infrastructure.transaction.persistence.TransactionRepository;

@Repository
public class TransactionPostgresGateway implements TransactionGateway {

    private final TransactionRepository repository;

    public TransactionPostgresGateway(final TransactionRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<Transaction> findById(final UUID id) {
        return this.repository.findById(id).map(TransactionEntity::toDomain);
    }

    @Override
    public List<Transaction> findAllByAccountId(final UUID accoundId) {
        return this.repository.findByAccountId(accoundId).stream().map(TransactionEntity::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public Transaction create(final Transaction transaction) {
        TransactionEntity newTransaction = this.repository.save(TransactionEntity.fromDomain(transaction));
        return newTransaction.toDomain();
    }

    @Override
    public void remove(final UUID id) {
        this.repository.deleteById(id);
    }

    @Override
    public void update(final Transaction transaction) {
        this.repository.save(TransactionEntity.fromDomain(transaction));
    }

    @Override
    public Pagination<Transaction> findAll(final TransactionSearchQuery query) {
        final var page = PageRequest.of(
            query.page(),
            query.perPage(),
            Sort.by(Direction.fromString(query.direction()), query.sort())
        );

        final var now = LocalDateTime.now().withMonth(query.month());

        final var startedAt = now.with(TemporalAdjusters.firstDayOfMonth())
            .withHour(0)
            .withMinute(0)
            .withSecond(0);
            
        final var finishAt = now.with(TemporalAdjusters.lastDayOfMonth())
            .withHour(23)
            .withMinute(59)
            .withSecond(59);
       
        final var whereClause = Specification.where(greaterThan(startedAt).and(lessThan(finishAt)));

        final var pageResult = this.repository.findAll(whereClause, page);

        return new Pagination<>(
            pageResult.getNumber(),
            pageResult.getSize(),
            pageResult.getTotalElements(),
            pageResult.map(TransactionEntity::toDomain).toList()
         );
    }

    private static Specification<TransactionEntity> lessThan(final LocalDateTime finishAt) {
        return (root, q, cb) -> cb.lessThan(root.get("date"), finishAt);
    }

    private static Specification<TransactionEntity> greaterThan(final LocalDateTime startedAt) {
        return (root, q, cb) -> cb.greaterThan(root.get("date"), startedAt);
    }

}
