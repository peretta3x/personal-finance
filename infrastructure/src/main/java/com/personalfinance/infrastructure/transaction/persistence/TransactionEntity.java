package com.personalfinance.infrastructure.transaction.persistence;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.*;

import com.personalfinance.domain.transaction.Transaction;
import com.personalfinance.domain.transaction.TransactionID;
import com.personalfinance.domain.transaction.TransactionType;
import com.personalfinance.infrastructure.account.persistence.AccountEntity;

@Entity
@Table(name = "transactions")
public class TransactionEntity {

    @Id
    private UUID id;

    @Column(name = "account_id", insertable = false, updatable = false)
    private UUID accountId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private AccountEntity account;

    @Column
    private String description;

    @Column(name = "category_id")
    private UUID categoryId;

    @Column
    private BigDecimal value;

    @Enumerated(EnumType.ORDINAL)
    private TransactionType type;

    @Column
    private LocalDateTime date;

    public TransactionEntity() {
    }

    public TransactionEntity(UUID id, UUID accountId, String description, UUID categoryId, BigDecimal value,
            TransactionType type, LocalDateTime date) {
        this.id = id;
        this.accountId = accountId;
        this.description = description;
        this.categoryId = categoryId;
        this.value = value;
        this.type = type;
        this.date = date;

        var account = new AccountEntity();
        account.setId(accountId);
        this.setAccount(account);
    }

    public UUID getId() {
        return this.id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getAccountId() {
        return this.accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UUID getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(UUID categoryId) {
        this.categoryId = categoryId;
    }

    public BigDecimal getValue() {
        return this.value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public TransactionType getType() {
        return this.type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public void setAccount(AccountEntity account) {
        this.account = account;
    }

    public AccountEntity getAccount() {
        return account;
    }

    public Transaction toDomain() {
        return Transaction.with(TransactionID.from(this.getId()), this.getAccountId(), this.getDescription(), this.getCategoryId(),
                this.getValue(), this.getType(), this.getDate());
    }

    public static TransactionEntity fromDomain(Transaction transaction) {
        return new TransactionEntity(UUID.fromString(transaction.getId().getValue()), transaction.getAccountId(), transaction.getDescription(),
                transaction.getCategoryId(), transaction.getValue(), transaction.getType(), transaction.getDate());
    }

}
