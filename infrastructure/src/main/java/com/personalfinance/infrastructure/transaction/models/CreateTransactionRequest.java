package com.personalfinance.infrastructure.transaction.models;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public record CreateTransactionRequest(
    @JsonProperty("user_id") UUID userId,
    String description,
    @JsonProperty("category_id") UUID categoryId,
    BigDecimal value,
    String type,
    LocalDateTime date
) {
    
}
