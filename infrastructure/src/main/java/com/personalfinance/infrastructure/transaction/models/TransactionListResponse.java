package com.personalfinance.infrastructure.transaction.models;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public record TransactionListResponse(
    UUID id,
    String description,
    BigDecimal value,
    @JsonProperty("category_id") UUID categoryId,
    String type,
    LocalDateTime date
    
) {
    
}
