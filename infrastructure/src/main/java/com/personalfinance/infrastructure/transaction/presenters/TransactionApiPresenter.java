package com.personalfinance.infrastructure.transaction.presenters;

import com.personalfinance.application.usecases.transaction.common.TransactionOutput;
import com.personalfinance.infrastructure.transaction.models.TransactionListResponse;

public interface TransactionApiPresenter {

    static TransactionListResponse present(final TransactionOutput output) {
        return new TransactionListResponse(
                output.id(),
                output.description(),
                output.value(),
                output.categoryId(),
                output.type().name(),
                output.date()
        );
    }
    
}
