package com.personalfinance.infrastructure.category.persistence;

import com.personalfinance.domain.category.Category;
import com.personalfinance.domain.category.CategoryID;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "categories")
public class CategoryEntity {

    @Id
    private UUID id;

    @Column
    private String name;

    public CategoryEntity() {
    }

    public CategoryEntity(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    public UUID getId() {
        return this.id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category toAggregate() {
        return Category.with(CategoryID.from(this.getId()), this.getName());
    }

    public static CategoryEntity fromDomain(Category category) {
        return new CategoryEntity(UUID.fromString(category.getId().getValue()), category.getName());
    }

}
