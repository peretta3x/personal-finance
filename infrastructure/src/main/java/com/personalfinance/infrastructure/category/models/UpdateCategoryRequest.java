package com.personalfinance.infrastructure.category.models;

public record UpdateCategoryRequest(
    String name
) {
    
}
