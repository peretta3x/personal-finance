package com.personalfinance.infrastructure.category.models;

public record CreateCategoryRequest(String name) {
}
