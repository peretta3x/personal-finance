package com.personalfinance.infrastructure.category;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import com.personalfinance.domain.category.Category;
import com.personalfinance.domain.category.CategoryGateway;
import com.personalfinance.infrastructure.category.persistence.CategoryEntity;
import com.personalfinance.infrastructure.category.persistence.CategoryRepository;

import org.springframework.stereotype.Repository;

@Repository
public class CategoryPostgresGateway implements CategoryGateway {

    private final CategoryRepository repository;

    public CategoryPostgresGateway(CategoryRepository categoryRepository) {
        this.repository = categoryRepository;
    }

    @Override
    public Optional<Category> findById(UUID id) {
        return repository.findById(id).map(CategoryEntity::toAggregate);
    }

    @Override
    public Category create(Category category) {
        var categoryEntity = CategoryEntity.fromDomain(category);
        return repository.save(categoryEntity).toAggregate();

    }

    @Override
    public Optional<Category> findByName(String name) {
        return repository.findByName(name).map(CategoryEntity::toAggregate);
    }

    @Override
    public List<Category> findAll() {
        return repository.findAll().stream()
                .map(CategoryEntity::toAggregate)
                .collect(Collectors.toList());
    }

    @Override
    public void remove(UUID id) {
        repository.deleteById(id);
    }

    @Override
    public void update(Category category) {
        var categoryEntity = CategoryEntity.fromDomain(category);
        repository.save(categoryEntity);
    }
}
