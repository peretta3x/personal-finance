CREATE TABLE accounts (
	id UUID NOT NULL,
	balance NUMERIC(19, 2) NOT NULL,
	user_id UUID NOT NULL,
	CONSTRAINT accounts_pkey PRIMARY KEY (id)
);

CREATE TABLE transactions (
	id UUID NOT NULL,
	account_id UUID NULL,
	category_id UUID NULL,
	"date" timestamp NULL,
	description VARCHAR(255) NULL,
	"type" VARCHAR(6) NULL,
	value numeric(19, 2) NULL,
	CONSTRAINT transactions_pkey PRIMARY KEY (id)
);

ALTER TABLE transactions ADD CONSTRAINT transactions_account_fk FOREIGN KEY (account_id) REFERENCES accounts(id);

CREATE TABLE categories (
	id UUID NOT NULL,
	"name" VARCHAR(255) NULL,
	CONSTRAINT categories_pkey PRIMARY KEY (id)
);