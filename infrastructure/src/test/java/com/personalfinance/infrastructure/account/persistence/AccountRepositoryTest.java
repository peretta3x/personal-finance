package com.personalfinance.infrastructure.account.persistence;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.personalfinance.infrastructure.PostgresGatewayTest;

@PostgresGatewayTest
public class AccountRepositoryTest {

    @Autowired
    private AccountRepository repository;

    @Test
    void shouldFindAll() {
        List<AccountEntity> accounts = this.repository.findAll().stream().toList();
        assertThat(accounts).isEmpty();
    }

}
