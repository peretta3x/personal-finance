package com.personalfinance.infrastructure.category.persistence;

import static org.assertj.core.api.Assertions.assertThat;

import com.personalfinance.domain.category.Category;
import com.personalfinance.infrastructure.PostgresGatewayTest;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

@PostgresGatewayTest
public class CategoryRepositoryTest {

    @Autowired
    private CategoryRepository repository;

    @BeforeEach
    void beforeEach() {
        this.repository.deleteAll();
    }

    @Test
    void shouldCreateNewCategory() {
        var category = Category.newCategory("Action");
        var input = CategoryEntity.fromDomain(category);

        var actual = repository.save(input);
        assertThat(actual).isNotNull();
        assertThat(actual).hasFieldOrPropertyWithValue("name", "Action");
    }

    @Test
    void shouldFindAllCategoriesAndReturnTwoAListWithTwoElements() {
        var category1 = Category.newCategory("Action");
        var input1 = CategoryEntity.fromDomain(category1);

        var category2 = Category.newCategory("Clothes");
        var input2 = CategoryEntity.fromDomain(category2);

        repository.save(input1);
        repository.save(input2);

        var categories = repository.findAll().stream().toList();
        assertThat(categories).isNotNull();
        assertThat(categories).hasSize(2);
    }

    @Test
    void shouldFindCategoryById() {
        Category category = Category.newCategory("Food");
        CategoryEntity input = CategoryEntity.fromDomain(category);

        var categoryCreated = repository.save(input);
        Optional<CategoryEntity> actual = repository.findById(categoryCreated.getId());

        assertThat(actual).isPresent();
        assertThat(actual.get()).hasFieldOrPropertyWithValue("name", "Food");
    }

}
