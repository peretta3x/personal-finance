package com.personalfinance.infrastructure.transaction.persistence;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.personalfinance.domain.transaction.TransactionType;
import com.personalfinance.infrastructure.PostgresGatewayTest;
import com.personalfinance.infrastructure.account.persistence.AccountEntity;
import com.personalfinance.infrastructure.account.persistence.AccountRepository;

@PostgresGatewayTest
public class TransactionRepositoryTest {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    private AccountEntity account = new AccountEntity(UUID.randomUUID(), UUID.randomUUID(), new BigDecimal(1000));

    @BeforeEach
    void beforeEach() {
        account = this.accountRepository.save(account);
    }

    @Test
    void shouldCreateNewTransaction() {
        var transaction = new TransactionEntity(UUID.randomUUID(), account.getId(), "desc", UUID.randomUUID(),
                BigDecimal.TEN,
                TransactionType.DEBIT, LocalDateTime.now());

        this.transactionRepository.save(transaction);

        List<TransactionEntity> transactions = this.transactionRepository.findByAccountId(account.getId());

        assertThat(transactions).hasSize(1);
        assertThat(transactions.get(0).getAccountId()).isEqualTo(account.getId());
    }

    @Test
    void shouldRemoveTransaction() {
        var transaction1 = new TransactionEntity(UUID.randomUUID(), account.getId(), "desc", UUID.randomUUID(),
                BigDecimal.TEN,
                TransactionType.DEBIT, LocalDateTime.now());
        var transaction2 = new TransactionEntity(UUID.randomUUID(), account.getId(), "desc", UUID.randomUUID(),
                BigDecimal.TEN,
                TransactionType.DEBIT, LocalDateTime.now());

        this.transactionRepository.saveAll(List.of(transaction1, transaction2));
        var transactions = this.transactionRepository.findByAccountId(account.getId());
        assertThat(transactions).hasSize(2);

        this.transactionRepository.deleteById(transactions.get(0).getId());
        var transactionsAfterDelete = this.transactionRepository.findByAccountId(account.getId());
        assertThat(transactionsAfterDelete).hasSize(1);
    }

    @Test
    void shouldUpdateTransaction() {
        var transaction = new TransactionEntity(UUID.randomUUID(), account.getId(), "desc", UUID.randomUUID(),
                BigDecimal.TEN,
                TransactionType.DEBIT, LocalDateTime.now());

        var newTransaction = this.transactionRepository.saveAndFlush(transaction);
        var transactionFound = this.transactionRepository.findById(newTransaction.getId());
        assertThat(transactionFound).isPresent();
        assertThat(transactionFound.get().getDescription()).isEqualTo("desc");

        transactionFound.get().setDescription("new desc");
        TransactionEntity updatedTransaction = this.transactionRepository.saveAndFlush(transactionFound.get());
        assertThat(updatedTransaction.getDescription()).isEqualTo("new desc");
    }

}
