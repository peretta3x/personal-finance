package com.personalfinance.infrastructure.transaction;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.personalfinance.domain.transaction.TransactionSearchQuery;
import com.personalfinance.domain.transaction.TransactionType;
import com.personalfinance.infrastructure.PostgresGatewayTest;
import com.personalfinance.infrastructure.account.persistence.AccountEntity;
import com.personalfinance.infrastructure.account.persistence.AccountRepository;
import com.personalfinance.infrastructure.transaction.persistence.TransactionEntity;
import com.personalfinance.infrastructure.transaction.persistence.TransactionRepository;

@PostgresGatewayTest
public class TransactionPostgresGatewayTest {

    @Autowired
    private TransactionPostgresGateway transactionGateway;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void shouldFindAllTransactionsOfTheCurrentMonth() {
        final var expectedTotal = 2;
        final var expectedTransactionDescription1 = "Salary";
        final var expectedTransactionDescription2 = "Supermarket";

        final var account = new AccountEntity(UUID.randomUUID(), UUID.randomUUID(), new BigDecimal(1000));
        accountRepository.saveAndFlush(account);

        final var today = LocalDateTime.now().withHour(10);

        final var transaction1 = new TransactionEntity(
            UUID.randomUUID(), 
            account.getId(), 
            expectedTransactionDescription2, 
            UUID.randomUUID(),
            BigDecimal.TEN,
            TransactionType.DEBIT, 
            today.plusMinutes(2)
        );

        final var transaction2 = new TransactionEntity(
            UUID.randomUUID(), 
            account.getId(), 
            "New clothes", 
            UUID.randomUUID(),
            BigDecimal.TEN,
            TransactionType.DEBIT, 
            today.minusMonths(1)
        );

        final var transaction3 = new TransactionEntity(
            UUID.randomUUID(), 
            account.getId(), 
            expectedTransactionDescription1, 
            UUID.randomUUID(),
            new BigDecimal(1000),
            TransactionType.CREDIT,
            today
        );

        transactionRepository.saveAllAndFlush(List.of(transaction1, transaction2, transaction3));

        final var query = new TransactionSearchQuery(0, 10, account.getId(), today.getMonth().getValue(), "date", "ASC");

        final var actualResult = transactionGateway.findAll(query);
        
        assertThat(actualResult.total())
            .isEqualTo(expectedTotal);

        assertThat(actualResult.items().get(0).getDescription())
            .isEqualTo(expectedTransactionDescription1);

        assertThat(actualResult.items().get(1).getDescription())
            .isEqualTo(expectedTransactionDescription2);
    }
    
}
