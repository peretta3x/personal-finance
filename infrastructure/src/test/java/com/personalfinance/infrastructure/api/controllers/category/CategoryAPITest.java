package com.personalfinance.infrastructure.api.controllers.category;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.personalfinance.application.usecases.category.common.CategoryOutput;
import com.personalfinance.application.usecases.category.create.CreateCategoryUseCase;
import com.personalfinance.application.usecases.category.findall.FindAllCategoriesUseCase;
import com.personalfinance.application.usecases.category.get.FindCategoryByIdUseCase;
import com.personalfinance.application.usecases.category.remove.RemoveCategoryUseCase;
import com.personalfinance.application.usecases.category.update.UpdateCategoryUseCase;
import com.personalfinance.domain.exception.DomainException;
import com.personalfinance.domain.exception.NotFoundException;
import com.personalfinance.infrastructure.ControllerTest;
import com.personalfinance.infrastructure.category.models.CreateCategoryRequest;
import com.personalfinance.infrastructure.category.models.UpdateCategoryRequest;

@ControllerTest(controllers = CategoryController.class)
class CategoryAPITest {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private CreateCategoryUseCase createCategoryUseCase;

    @MockBean
    private UpdateCategoryUseCase updateCategoryUseCase;

    @MockBean
    private RemoveCategoryUseCase removeCategoryUseCase;

    @MockBean
    private FindAllCategoriesUseCase findAllCategoriesUseCase;

    @MockBean
    private FindCategoryByIdUseCase findCategoryByIdUseCase;

    @Test
    void givenAValidaCategoryInput_whenCallsCreateCategory_shouldReturnCategoryOutput() throws Exception {
        // given
        final var expectedId = UUID.randomUUID();
        final var expectedName = "Food";

        final var aInput = new CreateCategoryRequest(expectedName);

        when(createCategoryUseCase.execute(any()))
                .thenReturn(new CategoryOutput(expectedId, expectedName));

        // when
        final var request = MockMvcRequestBuilders.post("/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(aInput));

        final var response = this.mvc.perform(request)
                .andDo(print());

        // then
        response.andExpect(status().isCreated())
                .andExpect(header().string("Content-Type", MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id", equalTo(expectedId.toString())))
                .andExpect(jsonPath("$.name", equalTo(expectedName)));

        verify(createCategoryUseCase, times(1))
                .execute(argThat(input -> Objects.equals(expectedName, input.name())));
    }

    @Test
    void givenAnInvalidCategoryInput_whenCallsCreateCategory_shouldReturnError() throws Exception {
        // given
        final var expectedName = " ";
        final var expectedErrorMessage = "name can not be blank";

        final var anInput = new CreateCategoryRequest(expectedName);

        when(createCategoryUseCase.execute(any()))
                .thenThrow(new DomainException(expectedErrorMessage));

        // when
        final var request = MockMvcRequestBuilders.post("/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(anInput));

        final var response = this.mvc.perform(request)
                .andDo(print());

        // then
        response.andExpect(status().isUnprocessableEntity())
                .andExpect(header().string("Content-Type", MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.message", equalTo(expectedErrorMessage)));

        verify(createCategoryUseCase, times(1))
                .execute(argThat(input -> Objects.equals(expectedName, input.name())));
    }

    @Test
    void givenAValidUpdateCategoryInput_whenCallsUpdateCategory_shouldReturnOk() throws Exception {
        // given
        final var expectedId = UUID.randomUUID();
        final var expectedName = "Food";

        final var aInput = new UpdateCategoryRequest(expectedName); 

        
        doNothing()
            .when(updateCategoryUseCase).execute(any());

        // when
        final var request = MockMvcRequestBuilders.put("/categories/" + expectedId.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(aInput));

        final var response = this.mvc.perform(request)
                .andDo(print());

        // then
        response.andExpect(status().isNoContent());

        verify(updateCategoryUseCase, times(1))
                .execute(argThat(input -> Objects.equals(expectedName, input.name())));
    }

    @Test
    void givenAValidDeleteInput_whenCallsDeleteCategory_shouldReturnOk() throws Exception {
        // given
        final var expectedId = UUID.randomUUID();
        
        doNothing()
            .when(removeCategoryUseCase).execute(any());

        // when
        final var request = MockMvcRequestBuilders.delete("/categories/" + expectedId.toString())
                .accept(MediaType.APPLICATION_JSON);

        final var response = this.mvc.perform(request)
                .andDo(print());

        // then
        response.andExpect(status().isNoContent());

        verify(removeCategoryUseCase, times(1))
                .execute(argThat(anId -> Objects.equals(anId, expectedId)));
    }

    @Test
    void givenAValidId_whenCallsGetCategory_shouldReturnOk() throws Exception {
        // given
        final var expectedId = UUID.randomUUID();
        final var expectedName = "Food";
        
        when(findCategoryByIdUseCase.execute(any()))
            .thenReturn(new CategoryOutput(expectedId, expectedName));

        // when
        final var request = MockMvcRequestBuilders.get("/categories/" + expectedId.toString())
                .accept(MediaType.APPLICATION_JSON);

        final var response = this.mvc.perform(request)
                .andDo(print());

        // then
        response.andExpect(status().isOk())
            .andExpect(header().string("Content-Type", MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id", equalTo(expectedId.toString())))
            .andExpect(jsonPath("$.name", equalTo(expectedName)));;

        verify(findCategoryByIdUseCase, times(1))
                .execute(argThat(anId -> Objects.equals(anId, expectedId)));
    }

    @Test
    void givenAnInvalidId_whenCallsGetCategory_shouldReturnNotFound() throws Exception {
        // given
        final var expectedId = UUID.randomUUID();
        final var expectedErrorMessage = "Category with id %s not found".formatted(expectedId.toString());
        
        when(findCategoryByIdUseCase.execute(any()))
            .thenThrow(new NotFoundException(expectedErrorMessage));

        // when
        final var request = MockMvcRequestBuilders.get("/categories/" + expectedId.toString())
                .accept(MediaType.APPLICATION_JSON);

        final var response = this.mvc.perform(request)
                .andDo(print());

        // then
        response.andExpect(status().isNotFound())
            .andExpect(jsonPath("$.message", equalTo(expectedErrorMessage)));

        verify(findCategoryByIdUseCase, times(1))
                .execute(argThat(anId -> Objects.equals(anId, expectedId)));
    }

    @Test
    void givenAValidRequest_whenCallsGetAllCategory_shouldReturnOk() throws Exception {
        // given
        final var expectedIdOne = UUID.randomUUID();
        final var expectedNameOne = "Food";

        final var expectedIdTwo = UUID.randomUUID();
        final var expectedNameTwo = "Food";
        
        when(findAllCategoriesUseCase.execute())
            .thenReturn(
                List.of(
                    new CategoryOutput(expectedIdOne, expectedNameOne),
                    new CategoryOutput(expectedIdTwo, expectedNameTwo)
                )
            );

        // when
        final var request = MockMvcRequestBuilders.get("/categories/" )
                .accept(MediaType.APPLICATION_JSON);

        final var response = this.mvc.perform(request)
                .andDo(print());

        // then
        response.andExpect(status().isOk())
            .andExpect(header().string("Content-Type", MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$.[0].id", equalTo(expectedIdOne.toString())))
            .andExpect(jsonPath("$.[0].name", equalTo(expectedNameOne)))
            .andExpect(jsonPath("$.[1].id", equalTo(expectedIdTwo.toString())))
            .andExpect(jsonPath("$.[1].name", equalTo(expectedNameTwo)));

        verify(findAllCategoriesUseCase, times(1))
                .execute();
    }
}
