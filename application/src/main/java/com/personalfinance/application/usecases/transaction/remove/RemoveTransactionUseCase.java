package com.personalfinance.application.usecases.transaction.remove;

import java.util.UUID;

import com.personalfinance.application.usecases.UnitUseCase;

public abstract class RemoveTransactionUseCase extends UnitUseCase<UUID> {

}
