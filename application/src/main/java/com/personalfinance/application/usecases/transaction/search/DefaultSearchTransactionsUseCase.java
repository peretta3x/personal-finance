package com.personalfinance.application.usecases.transaction.search;

import com.personalfinance.application.usecases.transaction.common.TransactionOutput;
import com.personalfinance.domain.pagination.Pagination;
import com.personalfinance.domain.transaction.TransactionGateway;
import com.personalfinance.domain.transaction.TransactionSearchQuery;

public class DefaultSearchTransactionsUseCase extends SearchTransactionUseCase {

    private final TransactionGateway transactionGateway;

    public DefaultSearchTransactionsUseCase(final TransactionGateway transactionGateway) {
        this.transactionGateway = transactionGateway;
    }

    @Override
    public Pagination<TransactionOutput> execute(final TransactionSearchQuery query) {
        return transactionGateway.findAll(query).map(TransactionOutput::from);
    }
    
}
