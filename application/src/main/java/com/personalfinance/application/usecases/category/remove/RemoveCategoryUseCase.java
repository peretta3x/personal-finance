package com.personalfinance.application.usecases.category.remove;

import java.util.UUID;

import com.personalfinance.application.usecases.UnitUseCase;

public abstract class RemoveCategoryUseCase extends UnitUseCase<UUID> {
}
