package com.personalfinance.application.usecases.transaction.findall;

import com.personalfinance.application.usecases.transaction.common.TransactionOutput;
import com.personalfinance.domain.transaction.TransactionGateway;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class DefaultFindAllTransactionsByAccountUseCase extends FindAllTransactionsByAccountUseCase {

    private final TransactionGateway transactionGateway;

    public DefaultFindAllTransactionsByAccountUseCase(final TransactionGateway transactionGateway) {
        this.transactionGateway = transactionGateway;
    }

    public List<TransactionOutput> execute(final FindAllTransactionsInput input) {
        return this.transactionGateway.findAllByAccountId(input.accountId()).stream()
                .map(transaction -> new TransactionOutput(UUID.fromString(transaction.getId().getValue()), transaction.getDescription(),
                        transaction.getCategoryId(), transaction.getValue(), transaction.getType(),
                        transaction.getDate()))
                .collect(Collectors.toList());
    }
}
