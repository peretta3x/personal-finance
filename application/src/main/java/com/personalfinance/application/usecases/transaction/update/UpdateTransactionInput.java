package com.personalfinance.application.usecases.transaction.update;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import com.personalfinance.domain.transaction.TransactionType;

public record UpdateTransactionInput(
        UUID id,
        String description,
        UUID categoryId,
        BigDecimal value,
        TransactionType type,
        LocalDateTime date) {
}
