package com.personalfinance.application.usecases.transaction.findall;

import java.util.List;

import com.personalfinance.application.usecases.UseCase;
import com.personalfinance.application.usecases.transaction.common.TransactionOutput;

public abstract class FindAllTransactionsByAccountUseCase
        extends UseCase<FindAllTransactionsInput, List<TransactionOutput>> {

}
