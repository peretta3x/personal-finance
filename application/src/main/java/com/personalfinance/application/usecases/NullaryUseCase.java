package com.personalfinance.application.usecases;

public abstract class NullaryUseCase<OUT> {

    public abstract OUT execute();

}
