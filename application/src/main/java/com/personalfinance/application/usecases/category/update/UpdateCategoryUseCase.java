package com.personalfinance.application.usecases.category.update;

import com.personalfinance.application.usecases.UnitUseCase;

public abstract class UpdateCategoryUseCase extends UnitUseCase<UpdateCategoryInput> {
}
