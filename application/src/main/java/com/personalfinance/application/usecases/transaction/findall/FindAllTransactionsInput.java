package com.personalfinance.application.usecases.transaction.findall;

import java.util.UUID;

public record FindAllTransactionsInput(UUID accountId) {
}
