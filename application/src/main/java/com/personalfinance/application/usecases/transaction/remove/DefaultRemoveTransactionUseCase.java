package com.personalfinance.application.usecases.transaction.remove;

import java.util.UUID;

import com.personalfinance.domain.account.AccountGateway;
import com.personalfinance.domain.account.AccountID;
import com.personalfinance.domain.exception.NotFoundException;
import com.personalfinance.domain.transaction.TransactionGateway;

public class DefaultRemoveTransactionUseCase extends RemoveTransactionUseCase {

    private final TransactionGateway transactionGateway;
    private final AccountGateway accountGateway;

    public DefaultRemoveTransactionUseCase(final TransactionGateway transactionGateway,
            final AccountGateway accountGateway) {
        this.transactionGateway = transactionGateway;
        this.accountGateway = accountGateway;
    }

    @Override
    public void execute(final UUID transactionId) {
        var transaction = this.transactionGateway.findById(transactionId)
                .orElseThrow(() -> new NotFoundException("Transaction with id %s not found", transactionId));

        var account = this.accountGateway.getById(AccountID.from(transaction.getAccountId())).get();

        account.removeTransaction(transaction);
        this.accountGateway.save(account);
        this.transactionGateway.remove(transactionId);
    }

}
