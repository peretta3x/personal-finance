package com.personalfinance.application.usecases.account.get;

import java.util.UUID;

import com.personalfinance.application.usecases.account.common.AccountOutput;
import com.personalfinance.domain.account.AccountGateway;
import com.personalfinance.domain.exception.NotFoundException;

public class DefaultGetAccountByUserIdUseCase extends GetAccountByUserIdUseCase {
    private final AccountGateway accountGateway;

    public DefaultGetAccountByUserIdUseCase(final AccountGateway accountGateway) {
        this.accountGateway = accountGateway;
    }

    @Override
    public AccountOutput execute(final UUID userId) {
        return accountGateway.getByUserId(userId)
            .map(AccountOutput::from)
            .orElseThrow(() -> new NotFoundException("Account with user id %s not found", userId));
    }
}
