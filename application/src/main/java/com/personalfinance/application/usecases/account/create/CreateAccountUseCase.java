package com.personalfinance.application.usecases.account.create;

import java.util.UUID;

import com.personalfinance.application.usecases.UseCase;
import com.personalfinance.application.usecases.account.common.AccountOutput;

public abstract class CreateAccountUseCase extends UseCase<UUID, AccountOutput> {
    
}
