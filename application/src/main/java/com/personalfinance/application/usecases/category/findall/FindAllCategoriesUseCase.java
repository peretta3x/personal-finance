package com.personalfinance.application.usecases.category.findall;

import java.util.List;

import com.personalfinance.application.usecases.NullaryUseCase;
import com.personalfinance.application.usecases.category.common.CategoryOutput;

public abstract class FindAllCategoriesUseCase extends NullaryUseCase<List<CategoryOutput>> {

}
