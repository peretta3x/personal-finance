package com.personalfinance.application.usecases.category.update;

import com.personalfinance.domain.category.Category;
import com.personalfinance.domain.category.CategoryGateway;
import com.personalfinance.domain.exception.NotFoundException;

public class DefaultUpdateCategoryUseCase extends UpdateCategoryUseCase {

    private final CategoryGateway categoryGateway;

    public DefaultUpdateCategoryUseCase(final CategoryGateway categoryGateway) {
        this.categoryGateway = categoryGateway;
    }

    @Override
    public void execute(final UpdateCategoryInput input) {
        Category category = this.categoryGateway.findById(input.id())
                .orElseThrow(() -> new NotFoundException("Category with id %s not found", input.id()));

        category.update(input.name());
        this.categoryGateway.update(category);
    }

}
