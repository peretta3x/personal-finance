package com.personalfinance.application.usecases.category.remove;

import java.util.UUID;

import com.personalfinance.domain.category.CategoryGateway;

public class DefaultRemoveCategoryUseCase extends RemoveCategoryUseCase {

    private final CategoryGateway categoryGateway;

    public DefaultRemoveCategoryUseCase(final CategoryGateway categoryGateway) {
        this.categoryGateway = categoryGateway;
    }

    @Override
    public void execute(final UUID id) {
        this.categoryGateway.remove(id);
    }
}
