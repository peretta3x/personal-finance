package com.personalfinance.application.usecases.category.create;

import com.personalfinance.application.usecases.category.common.CategoryOutput;
import com.personalfinance.domain.category.Category;
import com.personalfinance.domain.category.CategoryGateway;

public class DefaultCreateCategoryUseCase extends CreateCategoryUseCase {

    private final CategoryGateway categoryGateway;

    public DefaultCreateCategoryUseCase(final CategoryGateway categoryGateway) {
        this.categoryGateway = categoryGateway;
    }

    @Override
    public CategoryOutput execute(final CategoryInput input) {
        var categoryByName = this.categoryGateway.findByName(input.name());
        categoryByName.ifPresent(category -> {
            throw new IllegalArgumentException(
                    String.format("Category %s already exists", category.getName()));
        });
        var category = Category.newCategory(input.name());
        var createdCategory = this.categoryGateway.create(category);
        return CategoryOutput.from(createdCategory);
    }

}
