package com.personalfinance.application.usecases.category.create;

public record CategoryInput(String name) {

    public static CategoryInput with(final String name) {
        return new CategoryInput(name);
    }
}
