package com.personalfinance.application.usecases.transaction.common;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import com.personalfinance.domain.transaction.Transaction;
import com.personalfinance.domain.transaction.TransactionType;

public record TransactionOutput(
        UUID id,
        String description,
        UUID categoryId,
        BigDecimal value,
        TransactionType type,
        LocalDateTime date
) {
    public static TransactionOutput from(final Transaction aTransaction) {
        return new TransactionOutput(
                UUID.fromString(aTransaction.getId().getValue()),
                aTransaction.getDescription(),
                aTransaction.getCategoryId(),
                aTransaction.getValue(),
                aTransaction.getType(),
                aTransaction.getDate()
        );
    }
}
