package com.personalfinance.application.usecases.category.findall;

import static java.util.stream.Collectors.toList;

import com.personalfinance.application.usecases.category.common.CategoryOutput;
import com.personalfinance.domain.category.CategoryGateway;
import java.util.List;

public class DefaultFindAllCategoriesUseCase extends FindAllCategoriesUseCase {

    private final CategoryGateway categoryGateway;

    public DefaultFindAllCategoriesUseCase(final CategoryGateway categoryGateway) {
        this.categoryGateway = categoryGateway;
    }

    @Override
    public List<CategoryOutput> execute() {
        return this.categoryGateway.findAll().stream()
                .map(CategoryOutput::from)
                .collect(toList());
    }

}
