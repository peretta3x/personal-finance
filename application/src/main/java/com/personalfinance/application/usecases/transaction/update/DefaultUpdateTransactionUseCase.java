package com.personalfinance.application.usecases.transaction.update;

import com.personalfinance.domain.account.AccountGateway;
import com.personalfinance.domain.account.AccountID;
import com.personalfinance.domain.exception.NotFoundException;
import com.personalfinance.domain.transaction.Transaction;
import com.personalfinance.domain.transaction.TransactionGateway;

public class DefaultUpdateTransactionUseCase extends UpdateTransactionUseCase {

    private final TransactionGateway transactionGateway;
    private final AccountGateway accountGateway;

    public DefaultUpdateTransactionUseCase(final TransactionGateway transactionGateway,
            final AccountGateway accountGateway) {
        this.transactionGateway = transactionGateway;
        this.accountGateway = accountGateway;
    }

    @Override
    public void execute(final UpdateTransactionInput input) {
        var transaction = this.transactionGateway.findById(input.id())
                .orElseThrow(() -> new NotFoundException("Transaction with id %s not found", input.id()));

        final var accountId = AccountID.from(transaction.getAccountId());
        var account = this.accountGateway.getById(accountId).get();

        var updatedTransaction = Transaction.with(transaction.getId(), transaction.getAccountId(),
                input.description(), input.categoryId(), input.value(), input.type(), input.date());

        account.removeTransaction(transaction);
        account.addTransaction(updatedTransaction);

        this.transactionGateway.update(updatedTransaction);
        this.accountGateway.save(account);
    }

}
