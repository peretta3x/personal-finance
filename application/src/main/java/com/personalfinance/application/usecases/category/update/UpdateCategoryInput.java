package com.personalfinance.application.usecases.category.update;

import java.util.UUID;

public record UpdateCategoryInput(
        UUID id,
        String name) {
}
