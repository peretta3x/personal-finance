package com.personalfinance.application.usecases.account.create;

import java.util.UUID;

import com.personalfinance.application.usecases.account.common.AccountOutput;
import com.personalfinance.domain.account.Account;
import com.personalfinance.domain.account.AccountGateway;

public class DefaultCreateAccountUseCase extends CreateAccountUseCase{

    private AccountGateway accountGateway;

    public DefaultCreateAccountUseCase(final AccountGateway accountGateway) {
        this.accountGateway = accountGateway;
    }

    @Override
    public AccountOutput execute(final UUID userId) {
        final var account = accountGateway.create(Account.newAccount(userId));
        return AccountOutput.from(account);
    }
    
}
