package com.personalfinance.application.usecases.transaction.search;

import com.personalfinance.application.usecases.UseCase;
import com.personalfinance.application.usecases.transaction.common.TransactionOutput;
import com.personalfinance.domain.pagination.Pagination;
import com.personalfinance.domain.transaction.TransactionSearchQuery;

public abstract class SearchTransactionUseCase extends UseCase<TransactionSearchQuery, Pagination<TransactionOutput>> {
    
}
