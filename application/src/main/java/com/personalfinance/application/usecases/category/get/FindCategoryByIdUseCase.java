package com.personalfinance.application.usecases.category.get;

import java.util.UUID;

import com.personalfinance.application.usecases.UseCase;
import com.personalfinance.application.usecases.category.common.CategoryOutput;

public abstract class FindCategoryByIdUseCase extends UseCase<UUID, CategoryOutput> {

}
