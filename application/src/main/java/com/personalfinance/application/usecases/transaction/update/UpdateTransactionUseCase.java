package com.personalfinance.application.usecases.transaction.update;

import com.personalfinance.application.usecases.UnitUseCase;

public abstract class UpdateTransactionUseCase extends UnitUseCase<UpdateTransactionInput> {

}
