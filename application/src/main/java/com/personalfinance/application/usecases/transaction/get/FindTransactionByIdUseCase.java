package com.personalfinance.application.usecases.transaction.get;

import java.util.UUID;

import com.personalfinance.application.usecases.UseCase;
import com.personalfinance.application.usecases.transaction.common.TransactionOutput;

public abstract class FindTransactionByIdUseCase extends UseCase<UUID, TransactionOutput> {
}
