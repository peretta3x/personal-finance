package com.personalfinance.application.usecases.category.common;

import com.personalfinance.domain.category.Category;
import java.util.UUID;

public record CategoryOutput(UUID id, String name) {
    public static CategoryOutput from(final Category category) {
        return new CategoryOutput(UUID.fromString(category.getId().getValue()), category.getName());
    }
}
