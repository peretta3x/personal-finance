package com.personalfinance.application.usecases.category.get;

import com.personalfinance.application.usecases.category.common.CategoryOutput;
import com.personalfinance.domain.category.CategoryGateway;
import com.personalfinance.domain.exception.NotFoundException;
import java.util.UUID;

public class DefaultFindCategoryByIdUseCase extends FindCategoryByIdUseCase {

    private final CategoryGateway categoryGateway;

    public DefaultFindCategoryByIdUseCase(final CategoryGateway categoryGateway) {
        this.categoryGateway = categoryGateway;
    }

    @Override
    public CategoryOutput execute(final UUID id) {
        return this.categoryGateway.findById(id)
                .map(CategoryOutput::from)
                .orElseThrow(() -> new NotFoundException("Category with id %s not found", id));
    };
}
