package com.personalfinance.application.usecases.account.common;

import java.math.BigDecimal;
import java.util.UUID;

import com.personalfinance.domain.account.Account;

public record AccountOutput(
    UUID id,
    UUID userId,
    BigDecimal balance

) {
    public static AccountOutput from(final Account account) {
        return new AccountOutput(
            UUID.fromString(account.getId().getValue()), 
            account.getUserId(),
            account.getBalance()
        );
    };
}
