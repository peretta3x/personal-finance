package com.personalfinance.application.usecases.transaction.get;

import com.personalfinance.application.usecases.transaction.common.TransactionOutput;
import com.personalfinance.domain.exception.NotFoundException;
import com.personalfinance.domain.transaction.TransactionGateway;
import java.util.UUID;

public class DefaultFindTransactionByIdUseCase extends FindTransactionByIdUseCase {

    private final TransactionGateway transactionGateway;

    public DefaultFindTransactionByIdUseCase(final TransactionGateway transactionGateway) {
        this.transactionGateway = transactionGateway;
    }

    @Override
    public TransactionOutput execute(final UUID transactionId) {
        return this.transactionGateway.findById(transactionId)
                .map(transaction -> new TransactionOutput(UUID.fromString(transaction.getId().getValue()), transaction.getDescription(),
                        transaction.getCategoryId(), transaction.getValue(), transaction.getType(),
                        transaction.getDate()))
                .orElseThrow(() -> new NotFoundException("Transaction with id %s not found", transactionId));
    }

}
