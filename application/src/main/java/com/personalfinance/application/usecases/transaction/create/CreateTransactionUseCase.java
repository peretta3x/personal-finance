package com.personalfinance.application.usecases.transaction.create;

import com.personalfinance.application.usecases.UseCase;

public abstract class CreateTransactionUseCase extends UseCase<CreateTransactionInput, CreateTransactionOutput> {

}
