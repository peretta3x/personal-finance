package com.personalfinance.application.usecases.transaction.create;

import java.util.UUID;

import com.personalfinance.domain.account.AccountGateway;
import com.personalfinance.domain.category.CategoryGateway;
import com.personalfinance.domain.exception.NotFoundException;
import com.personalfinance.domain.transaction.Transaction;
import com.personalfinance.domain.transaction.TransactionGateway;

public class DefaultCreateTransactionUseCase extends CreateTransactionUseCase {

    private final AccountGateway accountGateway;
    private final CategoryGateway categoryGateway;
    private final TransactionGateway transactionGateway;

    public DefaultCreateTransactionUseCase(
            final AccountGateway accountGateway,
            final CategoryGateway categoryGateway,
            final TransactionGateway transactionGateway) {
        this.accountGateway = accountGateway;
        this.categoryGateway = categoryGateway;
        this.transactionGateway = transactionGateway;
    }

    public CreateTransactionOutput execute(final CreateTransactionInput input) {
        final var userAccount = this.accountGateway.getByUserId(input.userId());
        userAccount.orElseThrow(() -> new NotFoundException("Account not found."));

        final var category = this.categoryGateway.findById(input.categoryId());
        category.orElseThrow(() -> new NotFoundException("Category not found"));

        final var accountId = userAccount.get().getId();

        final var transaction = Transaction.newTransaction(UUID.fromString(accountId.getValue()), input.description(), input.categoryId(),
                input.value(), input.type(), input.date());

        var account = userAccount.get();
        account.addTransaction(transaction);

        this.transactionGateway.create(transaction);
        this.accountGateway.save(account);

        var transactionOutput = new CreateTransactionOutput(UUID.fromString(transaction.getId().getValue()), transaction.getDescription(),
                transaction.getCategoryId(), transaction.getValue(), transaction.getType(), transaction.getDate(),
                account.getBalance());

        return transactionOutput;
    }
}
