package com.personalfinance.application.usecases.category.create;

import com.personalfinance.application.usecases.UseCase;
import com.personalfinance.application.usecases.category.common.CategoryOutput;

public abstract class CreateCategoryUseCase extends UseCase<CategoryInput, CategoryOutput> {

}
