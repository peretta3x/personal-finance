package com.personalfinance.application.usecases.account.get;

import java.util.UUID;

import com.personalfinance.application.usecases.UseCase;
import com.personalfinance.application.usecases.account.common.AccountOutput;

public abstract class GetAccountByUserIdUseCase extends UseCase<UUID, AccountOutput> {
}
