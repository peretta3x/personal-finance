package com.personalfinance.application.usecases.transaction.get;

import static java.time.LocalDateTime.now;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.personalfinance.application.usecases.transaction.common.TransactionOutput;
import com.personalfinance.domain.exception.NotFoundException;
import com.personalfinance.domain.transaction.Transaction;
import com.personalfinance.domain.transaction.TransactionGateway;
import com.personalfinance.domain.transaction.TransactionID;
import com.personalfinance.domain.transaction.TransactionType;

public class FindTransactionByIdUseCaseTest {

    private final TransactionGateway transactionGateway = Mockito.mock(TransactionGateway.class);

    @Test
    void shouldFindTransactionById() {
        var transactionId = TransactionID.unique();
        var id = UUID.fromString(transactionId.getValue());
        var accountId = UUID.randomUUID();
        var transaction = Transaction.with(transactionId, accountId, "Desc", UUID.randomUUID(), BigDecimal.TEN,
                TransactionType.CREDIT, now());

        var usecase = new DefaultFindTransactionByIdUseCase(transactionGateway);

        when(transactionGateway.findById(id))
            .thenReturn(Optional.of(transaction));

        TransactionOutput output = usecase.execute(id);

        Assertions.assertThat(output.id()).isEqualTo(id);
        Assertions.assertThat(output.description()).isEqualTo(transaction.getDescription());
        Assertions.assertThat(output.categoryId()).isEqualTo(transaction.getCategoryId());
        Assertions.assertThat(output.value()).isEqualTo(transaction.getValue());
        Assertions.assertThat(output.date()).isEqualTo(transaction.getDate());
    }

    @Test
    void shouldThrowNotFoundExceptionWhenTransactionIsNotFound() {
        var transactionId = UUID.randomUUID();

        when(transactionGateway.findById(transactionId)).thenReturn(Optional.empty());

        var usecase = new DefaultFindTransactionByIdUseCase(transactionGateway);

        Assertions.assertThatThrownBy(() -> usecase.execute(transactionId)).isInstanceOf(NotFoundException.class);
    }

}
