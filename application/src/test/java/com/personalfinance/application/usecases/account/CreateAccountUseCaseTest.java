package com.personalfinance.application.usecases.account;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.personalfinance.application.usecases.account.create.DefaultCreateAccountUseCase;
import com.personalfinance.domain.account.Account;
import com.personalfinance.domain.account.AccountGateway;

class CreateAccountUseCaseTest {

    private final AccountGateway accountGateway = mock(AccountGateway.class);

    @Test
    void shouldCreateANewAccount() {
        final var createAccountUseCase = new DefaultCreateAccountUseCase(accountGateway);
        final var userId = UUID.randomUUID();

        final var account = Account.newAccount(userId);
        when(accountGateway.create(Mockito.any())).thenReturn(account);

        final var newAccount = createAccountUseCase.execute(userId);
        assertThat(newAccount).isNotNull();
        assertThat(newAccount.userId()).isEqualTo(userId);
        assertThat(newAccount.balance()).isEqualTo(BigDecimal.ZERO);
    }

}