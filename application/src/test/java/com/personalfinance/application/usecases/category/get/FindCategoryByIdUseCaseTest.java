package com.personalfinance.application.usecases.category.get;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.personalfinance.domain.category.Category;
import com.personalfinance.domain.category.CategoryGateway;
import com.personalfinance.domain.exception.NotFoundException;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;

public class FindCategoryByIdUseCaseTest {

    private final CategoryGateway categoryGateway = mock(CategoryGateway.class);
    private final DefaultFindCategoryByIdUseCase findCategoryByIdUseCase = new DefaultFindCategoryByIdUseCase(
            categoryGateway);

    @Test
    void shouldFindCategorybyId() {
        var category = Category.newCategory("Food");
        var expectedCategoryId = UUID.fromString(category.getId().getValue());
        when(categoryGateway.findById(expectedCategoryId)).thenReturn(Optional.of(category));

        var actual = findCategoryByIdUseCase.execute(expectedCategoryId);

        assertThat(actual).isNotNull();
        assertThat(actual.name()).isEqualTo(category.getName());
        assertThat(actual.id()).isEqualTo(expectedCategoryId);
    }

    @Test
    void shouldThrowNotFoundExceptionWhenCategoryDoesNotExists() {
        var category = Category.newCategory("Food");
        var expectedCategoryId = UUID.fromString(category.getId().getValue());

        assertThatThrownBy(() -> findCategoryByIdUseCase.execute(expectedCategoryId))
                .isInstanceOf(NotFoundException.class);
    }

}
