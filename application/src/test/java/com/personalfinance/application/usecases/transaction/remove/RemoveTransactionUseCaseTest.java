package com.personalfinance.application.usecases.transaction.remove;

import static java.time.LocalDateTime.now;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.personalfinance.domain.account.Account;
import com.personalfinance.domain.account.AccountGateway;
import com.personalfinance.domain.account.AccountID;
import com.personalfinance.domain.exception.NotFoundException;
import com.personalfinance.domain.transaction.Transaction;
import com.personalfinance.domain.transaction.TransactionGateway;
import com.personalfinance.domain.transaction.TransactionType;

public class RemoveTransactionUseCaseTest {

    private final TransactionGateway transactionGateway = Mockito.mock(TransactionGateway.class);
    private final AccountGateway accountGateway = mock(AccountGateway.class);

    @Test
    void shouldRemoveTransaction() {
        var accountId = AccountID.unique();
        var userId = UUID.randomUUID();
        var account = Account.with(accountId, userId, new BigDecimal(100));

        var transaction = Transaction.newTransaction(
            UUID.fromString(accountId.getValue()), 
            "desc", 
            UUID.randomUUID(), 
            BigDecimal.TEN, 
            TransactionType.CREDIT,
            now()
        );

        var id = UUID.fromString(transaction.getId().getValue());

        when(transactionGateway.findById(id)).thenReturn(Optional.of(transaction));
        when(accountGateway.getById(any())).thenReturn(Optional.of(account));

        var usecase = new DefaultRemoveTransactionUseCase(transactionGateway, accountGateway);

        usecase.execute(id);

        verify(transactionGateway, timeout(1)).remove(id);
        verify(accountGateway, timeout(1)).save(account);

        Assertions.assertThat(account.getBalance()).isEqualTo(new BigDecimal(90));
    }

    @Test
    void shouldThrowNotFoundExceptionWhenTransactionIsNotFound() {
        var transactionId = UUID.randomUUID();

        when(transactionGateway.findById(transactionId)).thenReturn(Optional.empty());

        var usecase = new DefaultRemoveTransactionUseCase(transactionGateway, accountGateway);

        Assertions.assertThatThrownBy(() -> usecase.execute(transactionId)).isInstanceOf(NotFoundException.class);
    }

}
