package com.personalfinance.application.usecases.category.update;

import static org.mockito.Mockito.*;

import com.personalfinance.domain.category.Category;
import com.personalfinance.domain.category.CategoryGateway;
import java.util.Optional;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class UpdateCategoryUseCaseTest {

    private final CategoryGateway categoryGateway = mock(CategoryGateway.class);
    private final DefaultUpdateCategoryUseCase updateCategoryUseCase = new DefaultUpdateCategoryUseCase(categoryGateway);

    @Test
    void shouldUpdateCategory() {
        var category = Category.newCategory("Food");
        var expectedCategoryId = UUID.fromString(category.getId().getValue());
        var input = new UpdateCategoryInput(expectedCategoryId, "Clothes");

        when(categoryGateway.findById(expectedCategoryId)).thenReturn(Optional.of(category));
        doNothing().when(categoryGateway).update(category);

        updateCategoryUseCase.execute(input);

        verify(categoryGateway, times(1)).findById(expectedCategoryId);
        verify(categoryGateway, times(1)).update(category);

        Assertions.assertThat(category.getName()).isEqualTo("Clothes");
    }

}
