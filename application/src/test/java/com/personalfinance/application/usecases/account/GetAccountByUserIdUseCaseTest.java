package com.personalfinance.application.usecases.account;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.personalfinance.application.usecases.account.get.DefaultGetAccountByUserIdUseCase;
import com.personalfinance.domain.account.Account;
import com.personalfinance.domain.account.AccountGateway;
import com.personalfinance.domain.exception.NotFoundException;

class GetAccountByUserIdUseCaseTest {

    private final AccountGateway accountGateway = Mockito.mock(AccountGateway.class);

    @Test
    void shouldReadAnAccountByItsUserId() {
        var userId = UUID.randomUUID();
        var account = Account.newAccount(userId);

        when(accountGateway.getByUserId(userId)).thenReturn(Optional.of(account));

        var readAccountUseCase = new DefaultGetAccountByUserIdUseCase(accountGateway);
        var accountFound = readAccountUseCase.execute(userId);

        assertThat(accountFound).isNotNull();
    }

    @Test
    void shouldReturnEmptyWhenAccountDoesNotExists() {
        final var userId = UUID.randomUUID();
        final var accountNotExistentId = UUID.randomUUID();
        final var expectedErrorMessage = "Account with user id %s not found";
        accountGateway.create(Account.newAccount(userId));

        final var readAccountUseCase = new DefaultGetAccountByUserIdUseCase(accountGateway);

        final var actualException = assertThrows(
            NotFoundException.class, 
            () -> readAccountUseCase.execute(accountNotExistentId)
        );

        assertThat(actualException.getMessage())
            .isEqualTo(expectedErrorMessage.formatted(accountNotExistentId));
    }
}