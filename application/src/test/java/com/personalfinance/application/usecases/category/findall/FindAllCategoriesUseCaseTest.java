package com.personalfinance.application.usecases.category.findall;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.personalfinance.application.usecases.category.common.CategoryOutput;
import com.personalfinance.domain.category.Category;
import com.personalfinance.domain.category.CategoryGateway;

public class FindAllCategoriesUseCaseTest {

    private final CategoryGateway categoryGateway = mock(CategoryGateway.class);

    @Test
    void shouldReturnAllCategories() {
        var category1 = Category.newCategory( "Food");
        var category2 = Category.newCategory("Shopping");

        when(categoryGateway.findAll()).thenReturn(List.of(category1, category2));

        var useCase = new DefaultFindAllCategoriesUseCase(categoryGateway);
        List<CategoryOutput> categories = useCase.execute();

        Assertions.assertThat(categories).hasSize(2);
        Assertions.assertThat(categories.get(0).id()).isNotNull();
        Assertions.assertThat(categories.get(0).name()).isEqualTo(category1.getName());
        Assertions.assertThat(categories.get(1).id()).isNotNull();
        Assertions.assertThat(categories.get(1).name()).isEqualTo(category2.getName());

    }

}
