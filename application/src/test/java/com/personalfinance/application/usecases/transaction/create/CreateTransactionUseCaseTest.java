package com.personalfinance.application.usecases.transaction.create;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import com.personalfinance.domain.account.Account;
import com.personalfinance.domain.account.AccountGateway;
import com.personalfinance.domain.category.Category;
import com.personalfinance.domain.category.CategoryGateway;
import com.personalfinance.domain.exception.NotFoundException;
import com.personalfinance.domain.transaction.Transaction;
import com.personalfinance.domain.transaction.TransactionGateway;
import com.personalfinance.domain.transaction.TransactionType;

public class CreateTransactionUseCaseTest {
    private final AccountGateway accountGateway = mock(AccountGateway.class);
    private final CategoryGateway categoryGateway = mock(CategoryGateway.class);
    private final TransactionGateway transactionGateway = mock(TransactionGateway.class);

    @Test
    void shouldCreateNewTransaction() {
        var userId = UUID.randomUUID();
        var categoryId = UUID.randomUUID();
        var account = Account.newAccount(userId);

        when(accountGateway.getByUserId(userId)).thenReturn(Optional.of(account));
        when(categoryGateway.findById(categoryId)).thenReturn(Optional.of(Category.newCategory("Test")));

        var createTransactionUserCase = new DefaultCreateTransactionUseCase(accountGateway, categoryGateway,
                transactionGateway);

        var input = new CreateTransactionInput(userId, "food", categoryId, BigDecimal.TEN,
                TransactionType.DEBIT, LocalDateTime.now());

        var output = createTransactionUserCase.execute(input);

        verify(accountGateway, times(1)).save(account);
        verify(transactionGateway, times(1)).create(any(Transaction.class));

        assertThat(output).isNotNull();
        assertThat(output.id()).isNotNull();
        assertThat(output.newBalance()).isEqualTo(BigDecimal.TEN.multiply(new BigDecimal(-1)));
    }

    @Test
    void shouldNotCreateNewTransactionWhenUserAccountIsNotFound() {
        DefaultCreateTransactionUseCase createTransactionUserCase = new DefaultCreateTransactionUseCase(
                accountGateway, categoryGateway, transactionGateway);

        var input = new CreateTransactionInput(UUID.randomUUID(), "food", UUID.randomUUID(), BigDecimal.TEN,
                TransactionType.DEBIT, LocalDateTime.now());

        assertThatThrownBy(() -> createTransactionUserCase.execute(input))
                .isInstanceOf(NotFoundException.class);
    }
}
