package com.personalfinance.application.usecases.transaction.findall;

import static com.personalfinance.domain.transaction.TransactionType.CREDIT;
import static com.personalfinance.domain.transaction.TransactionType.DEBIT;
import static java.time.LocalDateTime.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.personalfinance.application.usecases.transaction.common.TransactionOutput;
import com.personalfinance.domain.transaction.Transaction;
import com.personalfinance.domain.transaction.TransactionGateway;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;

public class FindAllTransactionsByAccountUseCaseTest {

    private final TransactionGateway transactionGateway = mock(TransactionGateway.class);

    @Test
    void shouldGetAllTransactionsByUseCase() {
        var usecase = new DefaultFindAllTransactionsByAccountUseCase(transactionGateway);
        var accountId = UUID.randomUUID();
        var input = new FindAllTransactionsInput(accountId);

        var categoryId = UUID.randomUUID();

        var transaction1 = Transaction.newTransaction(accountId, "desc", categoryId, BigDecimal.TEN, CREDIT, now());
        var transaction2 = Transaction.newTransaction(accountId, "desc 2", categoryId, BigDecimal.TEN, DEBIT, now());

        when(transactionGateway.findAllByAccountId(accountId)).thenReturn(List.of(transaction1, transaction2));

        List<TransactionOutput> transactionsOutput = usecase.execute(input);

        assertThat(transactionsOutput).hasSize(2);
        assertThat(transactionsOutput.get(0).type()).isEqualTo(CREDIT);
        assertThat(transactionsOutput.get(1).type()).isEqualTo(DEBIT);
    }
}
