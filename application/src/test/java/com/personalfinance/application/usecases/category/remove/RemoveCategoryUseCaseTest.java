package com.personalfinance.application.usecases.category.remove;

import static org.mockito.Mockito.*;

import com.personalfinance.domain.category.Category;
import com.personalfinance.domain.category.CategoryGateway;
import java.util.UUID;
import org.junit.jupiter.api.Test;

public class RemoveCategoryUseCaseTest {

    private final CategoryGateway categoryGateway = mock(CategoryGateway.class);
    private final DefaultRemoveCategoryUseCase removeCategoryUseCase = new DefaultRemoveCategoryUseCase(categoryGateway);

    @Test
    void shouldRemoveCategory() {
        var category = Category.newCategory("Food");
        var expectedCategoryId = UUID.fromString(category.getId().getValue());
        removeCategoryUseCase.execute(expectedCategoryId);
        verify(categoryGateway, times(1)).remove(expectedCategoryId);
    }

}
