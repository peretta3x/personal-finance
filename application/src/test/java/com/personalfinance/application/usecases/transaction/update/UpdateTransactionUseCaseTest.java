package com.personalfinance.application.usecases.transaction.update;

import static java.time.LocalDateTime.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import com.personalfinance.domain.account.Account;
import com.personalfinance.domain.account.AccountGateway;
import com.personalfinance.domain.account.AccountID;
import com.personalfinance.domain.transaction.Transaction;
import com.personalfinance.domain.transaction.TransactionGateway;
import com.personalfinance.domain.transaction.TransactionID;
import com.personalfinance.domain.transaction.TransactionType;

public class UpdateTransactionUseCaseTest {

    private final TransactionGateway transactionGateway = mock(TransactionGateway.class);
    private final AccountGateway accountGateway = mock(AccountGateway.class);

    @Test
    void shouldUpdateTransaction() {
        final var transactionId = TransactionID.unique();
        final var id = UUID.fromString(transactionId.getValue());
        final var accountId = AccountID.unique();

        final var transaction = Transaction.with(
            transactionId, 
            UUID.fromString(accountId.getValue()), 
            "Desc", 
            UUID.randomUUID(), 
            BigDecimal.TEN,
            TransactionType.CREDIT, 
            now()
        );
        
        final var account = Account.with(accountId, UUID.randomUUID(), BigDecimal.TEN);

        var usecase = new DefaultUpdateTransactionUseCase(transactionGateway, accountGateway);

        when(transactionGateway.findById(id))
            .thenReturn(Optional.of(transaction));
        when(accountGateway.getById(any()))
            .thenReturn(Optional.of(account));

        var newCreditValue = new BigDecimal(1000);

        var updateInput = new UpdateTransactionInput(id, "desc test", transaction.getCategoryId(),
                newCreditValue, TransactionType.CREDIT, now());

        usecase.execute(updateInput);

        verify(accountGateway, times(1)).save(account);
        verify(transactionGateway, times(1)).update(any(Transaction.class));

        assertThat(account.getBalance()).isEqualTo(newCreditValue);
    }

}
