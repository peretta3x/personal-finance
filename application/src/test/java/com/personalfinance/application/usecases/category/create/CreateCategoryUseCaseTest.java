package com.personalfinance.application.usecases.category.create;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.personalfinance.domain.category.Category;
import com.personalfinance.domain.category.CategoryGateway;
import java.util.Optional;
import org.junit.jupiter.api.Test;

public class CreateCategoryUseCaseTest {

    private final CategoryGateway categoryGateway = mock(CategoryGateway.class);

    @Test
    void shouldCreateANewCategory() {
        var useCase = new DefaultCreateCategoryUseCase(categoryGateway);
        var input = new CategoryInput("new category");

        when(categoryGateway.create(any(Category.class))).thenReturn(Category.newCategory("Food"));

        var output = useCase.execute(input);

        assertThat(output).isNotNull();
        assertThat(output.id()).isNotNull();
    }

    @Test
    void shouldThrowExceptionWhenCategoryWithSameNameAlreadyExists() {
        var useCase = new DefaultCreateCategoryUseCase(categoryGateway);
        var input = new CategoryInput("new category");
        when(categoryGateway.findByName(input.name())).thenReturn(Optional.of(Category.newCategory(input.name())));
        assertThatThrownBy(() -> useCase.execute(input)).isInstanceOf(IllegalArgumentException.class);
    }

}
