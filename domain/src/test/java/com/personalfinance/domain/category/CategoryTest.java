package com.personalfinance.domain.category;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.personalfinance.domain.exception.DomainException;
import org.junit.jupiter.api.Test;

public class CategoryTest {

    @Test
    void shouldInstantiateNewCategory() {
        Category category = Category.newCategory("Food");

        assertThat(category.getId()).isNotNull();
        assertThat(category.getName()).isEqualTo("Food");
    }

    @Test
    void shouldThrowNullPointerExceptionWhenNameIsNull() {
        assertThatThrownBy(() -> Category.newCategory(null))
                .isInstanceOf(DomainException.class);
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenNameIsNull() {
        assertThatThrownBy(() -> Category.newCategory(""))
                .isInstanceOf(DomainException.class);
    }

    @Test
    void givenInvalidNullName_whenCallUpdateCategory_thenReceiveDomainException() {
        final var expectedErrorMessage = "name is required";
        final var category = Category.newCategory("Food");

        assertThatThrownBy(() -> category.update(null))
                .isInstanceOf(DomainException.class)
                .hasMessage(expectedErrorMessage);
    }

    @Test
    void givenInvalidEmptyName_whenCallUpdateCategory_thenReceiveDomainException() {
        final var expectedErrorMessage = "name can not be blank";
        final var category = Category.newCategory("Food");

        assertThatThrownBy(() -> category.update(" "))
                .isInstanceOf(DomainException.class)
                .hasMessage(expectedErrorMessage);
    }

}
