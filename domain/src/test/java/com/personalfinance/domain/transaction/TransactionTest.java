package com.personalfinance.domain.transaction;

import static java.time.LocalDateTime.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.personalfinance.domain.category.Category;
import java.math.BigDecimal;
import java.util.UUID;
import org.junit.jupiter.api.Test;

class TransactionTest {

    private Category category = Category.newCategory("Category 1");

    @Test
    void shouldCreateNewTransactionInstance() {
        var accountId = UUID.randomUUID();
        var expectedCategoryId = UUID.fromString(category.getId().getValue());
        var transaction = Transaction.newTransaction(accountId, "description", expectedCategoryId, BigDecimal.TEN,
                TransactionType.CREDIT, now());
        assertThat(transaction).isNotNull();
        assertThat(transaction.getId()).isNotNull();
        assertThat(transaction.getDescription()).isEqualTo("description");
        assertThat(transaction.getCategoryId()).isEqualTo(expectedCategoryId);
        assertThat(transaction.getValue()).isEqualTo(BigDecimal.TEN);
        assertThat(transaction.getType()).isEqualTo(TransactionType.CREDIT);
    }

    @Test
    void shouldNotCreateNewTransactionInstanceWhenValueIsNull() {
        var accountId = UUID.randomUUID();
        assertThatThrownBy(
                () -> Transaction.newTransaction(accountId, "description", UUID.fromString(category.getId().getValue()), null, TransactionType.CREDIT, null))
                .isInstanceOf(NullPointerException.class);
    }

    @Test
    void shouldNotCreateNewTransactionInstanceWhenTypeIsNull() {
        var accountId = UUID.randomUUID();
        assertThatThrownBy(
                () -> Transaction.newTransaction(accountId, "description", UUID.fromString(category.getId().getValue()), BigDecimal.TEN, null, null))
                .isInstanceOf(NullPointerException.class);
    }

    @Test
    void shouldNotCreateNewTransactionInstanceWhenValueIsNegative() {
        var accountId = UUID.randomUUID();
        assertThatThrownBy(
                () -> Transaction.newTransaction(accountId, "description", UUID.fromString(category.getId().getValue()), new BigDecimal(-1), null, null))
                .isInstanceOf(NullPointerException.class);
    }

    @Test
    void shouldNotCreateNewTransactionInstanceWhenValueIsZero() {
        var accountId = UUID.randomUUID();
        assertThatThrownBy(
                () -> Transaction.newTransaction(accountId, "description", UUID.fromString(category.getId().getValue()), BigDecimal.ZERO, null, null))
                .isInstanceOf(NullPointerException.class);
    }

    @Test
    void shouldNotCreateNewTransactionInstanceWhenDateIsNull() {
        var accountId = UUID.randomUUID();
        assertThatThrownBy(
                () -> Transaction.newTransaction(accountId, "description", UUID.fromString(category.getId().getValue()), BigDecimal.ZERO,
                        TransactionType.CREDIT, null))
                .isInstanceOf(NullPointerException.class);
    }

}
