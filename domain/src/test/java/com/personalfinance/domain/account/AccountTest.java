package com.personalfinance.domain.account;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import com.personalfinance.domain.category.Category;
import com.personalfinance.domain.exception.DomainException;
import com.personalfinance.domain.transaction.Transaction;
import com.personalfinance.domain.transaction.TransactionType;

class AccountTest {

    private final UUID userId = UUID.randomUUID();
    private final Category category = Category.newCategory("Category 1");

    @Test
    void shouldCreateNewAccountInstance() {
        Account account = Account.newAccount(userId);
        assertThat(account).isNotNull();
        assertThat(account.getId()).isNotNull();
        assertThat(account.getUserId()).isEqualTo(userId);
        assertThat(account.getBalance()).isEqualTo(BigDecimal.ZERO);
    }

    @Test
    void shouldNotCreateNewAccountInstanceWhenUserIdIsNull() {
        assertThatThrownBy(() -> Account.newAccount(null))
                .isInstanceOf(DomainException.class);
    }

    @Test
    void shouldAddTransactionToAccount() {
        final var account = Account.newAccount(userId);
        final var accountId = UUID.fromString(account.getId().getValue());
        final var categoryId = UUID.fromString(category.getId().getValue());

        final var transaction = Transaction.newTransaction(
            accountId,
            "food", 
            categoryId, 
            BigDecimal.TEN,
            TransactionType.DEBIT, LocalDateTime.now()
        );

        account.addTransaction(transaction);

        assertThat(account.getBalance())
            .isEqualTo(new BigDecimal(-10));
        assertThat(account.getTransactions())
                .hasSize(1)
                .hasSameElementsAs(List.of(transaction));
    }

    @Test
    void shouldHaveBalanceEqual10WhenAddTransaction() {
        final var userId = UUID.randomUUID();
        final var account = Account.newAccount(userId);
        final var accountId = UUID.fromString(account.getId().getValue());
        final var categoryId = UUID.fromString(category.getId().getValue());

        final var transaction = Transaction.newTransaction(
            accountId, 
            "food", 
            categoryId, 
            BigDecimal.TEN,
            TransactionType.CREDIT,
            LocalDateTime.now()
        );

        account.addTransaction(transaction);

        assertThat(account.getTransactions())
                .hasSize(1)
                .hasSameElementsAs(List.of(transaction));
        
        assertThat(account.getBalance())
            .isEqualTo(BigDecimal.TEN);
    }

}
