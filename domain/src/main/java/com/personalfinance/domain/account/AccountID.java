package com.personalfinance.domain.account;

import java.util.UUID;

import com.personalfinance.domain.Identifier;

public class AccountID extends Identifier {

    private final String value;

    private AccountID(final String value) {
        this.value = value;
    }

    public static AccountID unique() {
        return AccountID.from(UUID.randomUUID());
    }

    public static AccountID from(final UUID anId) {
        return new AccountID(anId.toString().toLowerCase());
    }

    public static AccountID from(final String anId) {
        return new AccountID(anId);
    }

    @Override
    public String getValue() {
        return value;
    }
    
}
