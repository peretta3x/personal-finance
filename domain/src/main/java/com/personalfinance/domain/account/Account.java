package com.personalfinance.domain.account;

import java.math.BigDecimal;
import java.util.*;

import com.personalfinance.domain.AggregateRoot;
import com.personalfinance.domain.exception.DomainException;
import com.personalfinance.domain.transaction.Transaction;
import com.personalfinance.domain.transaction.TransactionType;

public class Account extends AggregateRoot<AccountID> {

    private UUID userId;
    private List<Transaction> transactions;
    private BigDecimal balance = BigDecimal.ZERO;

    private Account(final AccountID anId, final UUID userId, final BigDecimal balance) {
        super(anId);
        this.userId = userId;
        this.balance = balance;
        this.transactions = new ArrayList<>();
        selfValidation();
    }

    public static Account newAccount(final UUID userId) {
        final var anId = AccountID.unique();
        return new Account(anId, userId, BigDecimal.ZERO);
    }

    public static Account with(
        final AccountID anId, 
        final UUID userId,
        final BigDecimal balance
    ) {
        return new Account(anId, userId, balance);
    }

    private void selfValidation() {
        if (Objects.isNull(this.userId)) {
            throw new DomainException("User id must not be null");
        }
    }

    public UUID getUserId() {
        return userId;
    }

    public BigDecimal getBalance() {
        return this.balance;
    }

    public void addTransaction(Transaction transaction) {
        this.transactions.add(transaction);
        if (transaction.getType().equals(TransactionType.CREDIT)) {
            this.balance = this.balance.add(transaction.getValue());
        } else {
            this.balance = this.balance.subtract(transaction.getValue());
        }
    }

    public void removeTransaction(Transaction transaction) {
        if (transaction.getType().equals(TransactionType.CREDIT)) {
            this.balance = this.balance.subtract(transaction.getValue());
        } else {
            this.balance = this.balance.add(transaction.getValue());
        }
    }

    public List<Transaction> getTransactions() {
        return Collections.unmodifiableList(transactions);
    }
}