package com.personalfinance.domain.account;

import java.util.Optional;
import java.util.UUID;

public interface AccountGateway {
    Account create(Account account);

    Optional<Account> getById(AccountID id);

    Optional<Account> getByUserId(UUID userId);

    Account save(Account account);
}
