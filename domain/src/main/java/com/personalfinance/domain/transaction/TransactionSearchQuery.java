package com.personalfinance.domain.transaction;

import java.util.UUID;

public record TransactionSearchQuery(
    int page,
    int perPage,
    UUID accountId,
    int month,
    String sort,
    String direction
) {
    
}
