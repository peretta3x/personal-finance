package com.personalfinance.domain.transaction;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.personalfinance.domain.pagination.Pagination;

public interface TransactionGateway {

    Optional<Transaction> findById(UUID id);

    List<Transaction> findAllByAccountId(UUID accoundId);

    Pagination<Transaction> findAll(TransactionSearchQuery query);

    Transaction create(Transaction transaction);

    void remove(UUID id);

    void update(Transaction transaction);
}
