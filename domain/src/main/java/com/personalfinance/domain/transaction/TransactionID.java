package com.personalfinance.domain.transaction;

import java.util.UUID;

import com.personalfinance.domain.Identifier;

public class TransactionID extends Identifier {

    private final String value;

    private TransactionID(final String value) {
        this.value = value;
    }

    public static TransactionID unique() {
        return TransactionID.from(UUID.randomUUID());
    }

    public static TransactionID from(final UUID anId) {
        return new TransactionID(anId.toString().toLowerCase());
    }

    @Override public String getValue() {
        return value;
    }
}
