package com.personalfinance.domain.transaction;

import com.personalfinance.domain.AggregateRoot;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

public class Transaction extends AggregateRoot<TransactionID> {
    private UUID accountId;
    private String description;
    private UUID categoryId;
    private BigDecimal value;
    private TransactionType type;
    private LocalDateTime date;

    private void validate() {
        Objects.requireNonNull(this.value, "Value is required.");
        Objects.requireNonNull(this.type, "Type is required");
        Objects.requireNonNull(this.date, "Date is required");

        if (BigDecimal.ZERO.compareTo(this.value) >= 0) {
            throw new IllegalArgumentException("Value must be greater than zero.");
        }
    }

    private Transaction(
        final TransactionID id,
        final UUID accountId,
        final String description,
        final UUID categoryId,
        final BigDecimal value,
        final TransactionType type,
        final LocalDateTime date
    ) {
        super(id);
        this.accountId = accountId;
        this.description = description;
        this.categoryId = categoryId;
        this.type = type;
        this.value = value;
        this.date = date;
        validate();
    }

    public static Transaction newTransaction(
        final UUID accountId,
        final String description,
        final UUID categoryId,
        final BigDecimal value,
        final TransactionType type,
        final LocalDateTime date
    ) {
        final var id = TransactionID.unique();
        return new Transaction(
            id,
            accountId,
            description,
            categoryId,
            value,
            type,
            date
        );
    }

    public static Transaction with(
        final TransactionID id,
        final UUID accountId,
        final String description,
        final UUID categoryId,
        final BigDecimal value,
        final TransactionType type,
        final LocalDateTime date
    ) {
        return new Transaction(
            id,
            accountId,
            description,
            categoryId,
            value,
            type,
            date
        );
    }


    public TransactionID getId() {
        return id;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public String getDescription() {
        return this.description;
    }

    public BigDecimal getValue() {
        return value;
    }

    public TransactionType getType() {
        return type;
    }

    public UUID getCategoryId() {
        return categoryId;
    }

    public LocalDateTime getDate() {
        return date;
    }
}
