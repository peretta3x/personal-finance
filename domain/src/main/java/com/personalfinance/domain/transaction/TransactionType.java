package com.personalfinance.domain.transaction;

public enum TransactionType {
    CREDIT, DEBIT
}
