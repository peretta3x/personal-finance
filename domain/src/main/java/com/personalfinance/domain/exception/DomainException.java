package com.personalfinance.domain.exception;

public class DomainException extends NoStackTraceException {

    public DomainException(String message) {
        super(message);
    }
}
