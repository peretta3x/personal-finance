package com.personalfinance.domain.category;

import com.personalfinance.domain.AggregateRoot;
import com.personalfinance.domain.exception.DomainException;
import java.util.Objects;

public class Category extends AggregateRoot<CategoryID> {

    private String name;

    private Category(final CategoryID anId, final String name) {
        super(anId);
        this.name = name;

        selfValidation(name);
    }

    public static Category newCategory(final String name) {
        final var id = CategoryID.unique();
        return new Category(id, name);
    }

    public static Category with(
            final CategoryID id,
            final String name
    ){
        return new Category(
                id,
                name
        );
    }

    private void selfValidation(String name) {
        if (Objects.isNull(name)) {
            throw new DomainException("name is required");
        }

        if (this.name.isBlank()) {
            throw new DomainException("name can not be blank");
        }
    }

    public CategoryID getId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public void update(String name) {
        this.name = name;
        selfValidation(name);
    }

}
