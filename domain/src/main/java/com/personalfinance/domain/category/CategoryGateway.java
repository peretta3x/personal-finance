package com.personalfinance.domain.category;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CategoryGateway {

    Optional<Category> findById(UUID uuid);

    Category create(Category category);

    Optional<Category> findByName(String name);

    List<Category> findAll();

    void remove(UUID id);

    void update(Category category);

}
