package com.personalfinance.domain;

public abstract class Identifier extends ValueObject {

    public abstract String getValue();
}
