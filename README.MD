# Personal Finance

## Search options

### Find by account
Returns the account data with all transaction

Endpoint: /account/{id}

### Find by month
Returns the account data with all transactions of the given month

Endpoint: /account/{id}/{month}

### Find by period
Returns the account data with all transactions of the given period

Endpoint: /account/{id}/{startAt}/{endAt}